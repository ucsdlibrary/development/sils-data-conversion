# SILS Data Conversion

This script manipulates the data in the extract file before uploading it into the Ex Libris tool.  It scans through the extract file and when a preferred name is identified, flip flop the data with the legal name values.

### Docker
1. Build docker image `make build`
1. Run convert script `make run > <path_to_output_file/sils_output.txt>`
1. Run test `make full_test`
1. Run bundle update `make update`
1. Run rubocop 'make lint'
