#!/usr/bin/env ruby
# frozen_string_literal: false

require 'csv'

# convert csv data
class SilsData
  def convert
    header = true
    csv_output = ''
    file_name = 'tmp.txt'

    create_temp_file(file_name)   
    CSV.foreach(file_name, headers: true, encoding: 'ISO-8859-1') do |row|
      csv_output.concat("#{generate_header(row.to_hash)}\n") if header
      csv_output.concat("#{generate_csv(row.to_hash)}\n")
      header = false
    end
    csv_output.gsub('!;!','";"')
  rescue CSV::MalformedCSVError => e
    e.message
  end

  def create_temp_file(file_name)
    file = File.open(ENV['FILE'])
    data = file.read
    file.close
    File.open(file_name, "w") { |f| f.write data.gsub!('";"','!;!') }  
  end

  def generate_csv(row)
    name = row['PATRN NAME']
    legal_name = row['LEGAL NAME']
    if name != '' && legal_name != ''
      row['LEGAL NAME'] = name
      row['PATRN NAME'] = legal_name
    end
    generate_body(row)
  end

  def generate_header(row)
    row.keys.map { |i| "\"#{i}\"" }.join(',')
  end

  def generate_body(row)
    row.values.map { |i| "\"#{i}\"" }.join(',')
  end
end

puts SilsData.new.convert
