ARG RUBY_VERSION=2.7.1

FROM ruby:$RUBY_VERSION-alpine as production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  less \
  vim \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV FILE=sils_input.txt

# Install gems
COPY Gemfile* /app/
RUN bundle config set --local path 'vendor/bundle' && \
      gem update bundler && \
      bundle install --jobs "$(nproc)" --retry 2

COPY . /app
