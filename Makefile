# Makefile for common billing-ill development tasks

menu:
	@echo 'build: Run docker build'
	@echo 'run: Run convert script'
	@echo 'full_test: Run full minitest test suite'
	@echo 'update: Update the Gemfile.lock file'
	@echo 'lint: Run rubocop to lint all files in the project'

build:
	@echo 'Building the image'
	@docker build -t sils_convert:latest .

run:
	@docker run --rm sils_convert ruby lib/sils_convert.rb

update:
	@echo 'Updating the Gemfile.lock file'
	@docker run --rm --mount type=bind,source="$(shell pwd)",target=/app sils_convert bundle update

lint:
	@echo 'Running rubocop'
	@docker run --rm sils_convert bundle exec rubocop

full_test:
	@echo 'Running full test suites'
	@docker run --rm sils_convert rake
