require 'test_helper'

class SilsConvertTest < Minitest::Test
  def setup
    @data_object = SilsData.new
  end

  def test_convert
    assert_includes @data_object.convert, 'a12345678'
  end
end
